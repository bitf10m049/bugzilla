class CreateBugs < ActiveRecord::Migration
  def change
    create_table :bugs do |t|
      t.string :title
      t.text :detail
      t.integer :owner_id

      t.timestamps null: false
    end
  end
end
