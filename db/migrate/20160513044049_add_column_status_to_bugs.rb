class AddColumnStatusToBugs < ActiveRecord::Migration
  def change
    add_column :bugs, :status, :integer, index: true
  end
end
