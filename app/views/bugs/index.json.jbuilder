json.array!(@bugs) do |bug|
  json.extract! bug, :id, :title, :detail, :owner_id
  json.url bug_url(bug, format: :json)
end
