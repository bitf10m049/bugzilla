class User < ActiveRecord::Base
  has_many :bugs, foreign_key: :owner_id

  def full_name
    [first_name, last_name].join(' ')
  end
end

