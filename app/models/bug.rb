class Bug < ActiveRecord::Base
  belongs_to :owner, class_name: 'User'

  enum status: [:open, :assign, :resolve,:close]

  def next_possible_status
     case status.to_sym
        when :open
          %w(open assign resolve)
        when :assign
          %w(assign resolve)
        when :resolve
          %w(resolve close)
        when :close
          %w(close open)
        else
          %w(open assign resolve close)
     end
  end
end
